var VideoReporter = require('protractor-video-reporter');
const path = require('path');
const ffmpeg = require("ffmpeg-cli");
exports.config = {

  directConnect: true,
  // sauceUser: "applaudito" ,
  // sauceKey: "8af04450-4530-41af-a15f-51e7da5f04f1" ,
  // sauceRegion: "us-west-1" ,
  framework: 'jasmine',
  capabilities: {
     'browserName': 'firefox' ,
    },
  
  beforeLaunch: function () {
    require('ts-node').register({
      project: '../tsconfig.json'
    });
  },
  onPrepare: async () => {
    var globals = require("protractor");

    var VideoReporter = require('protractor-video-reporter');

    browser.driver.get("https://maui-dev.value-cloud.com/");
    browser.manage().window().maximize();
    //Only for non-angular applications
    await browser.waitForAngularEnabled(true);
    //browser.ignoreSynchronization = false;
    //Until herezo
    global.browser = globals.browser;

    var AllureReporter = require('jasmine-allure-reporter');
    jasmine.getEnv().addReporter(new AllureReporter({
      resultsDir: 'allure-results'
    }));
//VIDEO
    VideoReporter.prototype.jasmineStarted = function() {
      var self = this;
      if (self.options.singleVideo) {
        var videoPath = path.join(self.options.baseDirectory, 'testVideo.avi');

        self._startScreencast(videoPath);

      }
    }; 

    jasmine.getEnv().addReporter(
      new VideoReporter({
          baseDirectory: path.normalize(path.join(__dirname, './testingVideo/')),
          createSubtitles: false,
          singleVideo: true,
          ffmpegCmd: path.normalize(ffmpeg.path),
          ffmpegArgs: [
              '-f', 'gdigrab',
              '-framerate', '24',
              '-video_size', '1920x1080',
              '-i', 'desktop',
              '-q:v','10',
          ]
        }));


//VIDEO

        jasmine.getEnv().afterEach(function(done){
      browser.takeScreenshot().then(function (png) {
        allure.createAttachment('Screenshot', function () {
          return new Buffer(png, 'base64')
        }, 'image/png')();
        done();
      })
    });

 
  },


  specs: ['test/LoginFlow.ts', 'test/CaseStudieFlow.ts'],

  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true, defaultTimeoutInterval: 80000// Use colors in the command line report.
  }

}