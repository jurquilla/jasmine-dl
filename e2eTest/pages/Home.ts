import { browser, by, element, ExpectedConditions, protractor } from "protractor";
import { ElemenstExtensions } from "../utilities/ElementsExtensions";


export class Home {

  static variables(locator: string) {
    let elementF;
    if (locator == "TabCaseStudies") {
      elementF = ElemenstExtensions.highlightElement(element(by.xpath("//a[@id='topnav-Case Studies']")));
    }
    if (locator == "AddCaseStudie") {
      elementF = ElemenstExtensions.highlightElement(element(by.xpath("//*[contains(text(),' Add New ')]")));
    }
    if (locator == "Alert") {
      elementF = ElemenstExtensions.highlightElement(element(by.tagName("ngb-alert")));
    }
    if (locator == "Options") {
      elementF = element.all(by.xpath("//span[@class='dropdown-toggle']")).first()
    }
    if (locator == "Order") {
      elementF = ElemenstExtensions.highlightElement(element.all(by.xpath("//th[contains(text(),'Date Created')]")).first());
      
    }
    if (locator == "Delete") {
     elementF = ElemenstExtensions.highlightElement(element.all(by.linkText("Delete")).first());
      
    }
    if (locator == "Confirm") {
      elementF = ElemenstExtensions.highlightElement(element.all(by.xpath("//button[contains(text(),'Confirm')]")).first());
    }

    return elementF;
  }

  static createCaseStudy(redact: string, account: string, valueopt: string, name: string) {

    if (redact === "YES") {
      ElemenstExtensions.highlightElement(element.all(by.xpath("//label")).first()).click();
    }

    ElemenstExtensions.highlightElement(element.all(by.xpath("//input[@placeholder='Search']")).last()).sendKeys(account);

    ElemenstExtensions.highlightElement(element.all(by.linkText("Select")).first()).click();

    ElemenstExtensions.highlightElement(element(by.css("#models"))).click();

    ElemenstExtensions.highlightElement(element(by.xpath("//option[@value='" + valueopt + "']"))).click();

    ElemenstExtensions.highlightElement(element(by.id("caseStudyName"))).clear().sendKeys(name);

    ElemenstExtensions.highlightElement(element(by.css("[class='btn btn-primary']"))).click();

    //ElemenstExtensions.highlightElement(element.all(by.xpath("//*[contains(text(),' Add ')]")).last()).click();

  }

  static addTestimonial(firstName: string, lastName: string, testimonial: string) {

    ElemenstExtensions.highlightElement(element.all(by.xpath("//*[contains(text(),' Add new ')]")).get(0)).click();

    ElemenstExtensions.highlightElement(element(by.id("firstName"))).sendKeys(firstName);

    ElemenstExtensions.highlightElement(element(by.id("lastName"))).sendKeys(lastName);

    ElemenstExtensions.highlightElement(element(by.id("testimonial"))).sendKeys(testimonial);

    ElemenstExtensions.highlightElement(element.all(by.xpath("//*[contains(text(),'Add')]")).last()).click();

  }

  static addSection(valueopt: string, description: string, url: string, mediaDescription: string) {

    ElemenstExtensions.highlightElement(element.all(by.xpath("//*[contains(text(),' Add new ')]")).get(1)).click();

    ElemenstExtensions.highlightElement(element(by.id("sectionType"))).click();

    ElemenstExtensions.highlightElement(element(by.xpath("//option[@value='" + valueopt + "']"))).click();

    ElemenstExtensions.highlightElement(element(by.id("description"))).sendKeys(description);

    ElemenstExtensions.highlightElement(element(by.id("videoUrl"))).click();

    ElemenstExtensions.highlightElement(element(by.id("videoUrlAddress"))).sendKeys(url);

    ElemenstExtensions.highlightElement(element(by.id("mediaDescription"))).sendKeys(mediaDescription);

    ElemenstExtensions.highlightElement(element.all(by.xpath("//*[contains(text(),'Add')]")).last()).click();

  }

  static addBenefit(benefit: string) {

    ElemenstExtensions.highlightElement(element.all(by.xpath("//*[contains(text(),' Add new ')]")).get(2)).click();

    ElemenstExtensions.highlightElement(element(by.xpath("//select2/div[2]"))).click();

    ElemenstExtensions.highlightElement(element.all(by.xpath("//input")).last()).sendKeys(benefit);

    ElemenstExtensions.highlightElement(element.all(by.xpath("//ul[@class='select2-results__options']/li[1]")).first()).click();

    //browser.actions().sendKeys(protractor.Key.ENTER).perform();

    ElemenstExtensions.highlightElement(element.all(by.xpath("//*[contains(text(),'Add')]")).last()).click();

  }


  static cleanUp(loop: number) {
    let i;
    for (i = 0; i < loop; i++){
      ElemenstExtensions.highlightElement(this.variables("Options")).click();
      ElemenstExtensions.highlightElement(this.variables("Delete")).click();
  }
    this.variables("TabCaseStudies").click();

    //ElemenstExtensions.highlightElement(this.variables("Search")).sendKeys(search + " ");

    ElemenstExtensions.highlightElement(this.variables("Order")).click();
    ElemenstExtensions.highlightElement(this.variables("Order")).click();


    ElemenstExtensions.highlightElement(this.variables("Options")).click();

    ElemenstExtensions.highlightElement(element.all(by.xpath("//div[@class='dropdown-menu show']/a[text()=' Delete ']")).first()).click();
    //ElemenstExtensions.highlightElement(this.variables("Delete")).click();

    ElemenstExtensions.highlightElement(this.variables("Confirm")).click();
    //expect(this.variables("Alert").isDisplayed()).toBeTruthy();
   // await browser.sleep(6000);
    //browser.wait(ExpectedConditions.visibilityOf(this.variables("Alert")), 3000, 'Element taking too long to appear in the DOM');

}
};
