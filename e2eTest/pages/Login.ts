import { browser, by, element, ExpectedConditions } from "protractor";
import { ElemenstExtensions } from "../utilities/ElementsExtensions";


export class Login {

  static login(user: string, password: string) {
    ElemenstExtensions.highlightElement(element(by.css("#email"))).sendKeys(user);
    ElemenstExtensions.highlightElement(element(by.xpath("//input[@id='password']"))).sendKeys(password);
    ElemenstExtensions.highlightElement(element(by.xpath("//button[contains(text(),'Log In')]"))).click();

  };

  static variables(locator: string) {
    let elementF;
    if (locator = "DashBoard") {
      elementF = ElemenstExtensions.highlightElement(element(by.xpath("//li[@class='breadcrumb-item active']//a[contains(text(),'Dashboard')]")));
    }
    return elementF;
  }

};
