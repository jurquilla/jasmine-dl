import { browser } from "protractor";


export class ElemenstExtensions {

  static highlightElement(element: any) {

    browser.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 3px solid red;');", element);
    return element;

  };

};
