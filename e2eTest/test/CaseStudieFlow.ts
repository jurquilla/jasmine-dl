//Usually import on TS
import { Home } from "../pages/Home";


//describe('Case Studie', function () {
  describe('Case Studie', async () => {

  let name = "Test Case Study Protractor";

  it('Click on tab Case Studies', async () => {

    Home.variables("TabCaseStudies").click();

  });

  it('Click on (+ Add New) Case Studie', async () => {

    Home.variables("AddCaseStudie").click();
    //expect(Home.variables("Alert").isDisplayed()).toBeTruthy();

  });

  it('Creating new Case Study', async () => {


    Home.createCaseStudy("YES", "price", "625", name);
    expect(Home.variables("Alert").isDisplayed()).toBeTruthy();


  });


  it('Adding Testimonials', async () => {
    Home.addTestimonial("Josue", "Urquilla", "Test Testimony!!!");
    expect(Home.variables("Alert").isDisplayed()).toBeTruthy();

  

  });

  it('Adding Sections', async () => {
    Home.addSection("110", "Test Description!!!", "https://www.youtube.com/watch?v=7-IYx7kS8Ww", "Media Description!!!");
    expect(Home.variables("Alert").isDisplayed()).toBeTruthy();
 
  });
  

  it('Adding Benefits', async () => {
    Home.addBenefit("churn");
    expect(Home.variables("Alert").isDisplayed()).toBeTruthy();

  });
  
  
  	
  afterAll(async () => {
    Home.cleanUp(3);
    expect(Home.variables("Alert").isDisplayed()).toBeTruthy();
  });

});
