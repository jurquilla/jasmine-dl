exports.config = {
    directConnect: true,
    capabilities: {
      browserName: 'chrome',
    
      chromeOptions: {
         args: [ "--headless", "--disable-gpu"]
       }
    },
    framework: 'jasmine',
    beforeLaunch: function () {
        require('ts-node').register({
            project: '../tsconfig.json'
        });
    },

    specs: ['test/apiTest01.ts'],

    onPrepare: () => {

      var AllureReporter = require('jasmine-allure-reporter');
      jasmine.getEnv().addReporter(new AllureReporter({
        resultsDir: 'allure-results'
      }));

  },
  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true, defaultTimeoutInterval: 30000// Use colors in the command line report.
  }

}